# README #

This repository is highly outdated and just a playground of my youth.

### What is this repository for? ###

This program is a clone of the popular snake game using ncurses.

### How do I get set up? ###

You need ncurses in order to compile the program.
Just type ./compile.sh in order to compile it. You'll be asked whether
you want to compile in debug or release-mode.

### Who do I talk to? ###

* Repo owner or admin