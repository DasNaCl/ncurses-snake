#!/bin/bash

function debug
{
	mkdir -p bin
	mkdir -p bin/Debug
	g++ -ggdb -lncurses -std=c++11 src/main.cpp src/Application.cpp src/Snake.cpp src/Part.cpp
	mv a.out bin/Debug/snake
}

function release
{
	mkdir -p bin
	mkdir -p bin/Release
	g++ -O2 -s -lncurses -std=c++11 src/main.cpp src/Application.cpp src/Snake.cpp src/Part.cpp
	mv a.out bin/Release/snake
}

if [ $(dpkg-query -W -f='${Status}' libncurses5 2>/dev/null | grep -c "ok installed") == 0 ];
then
	echo "ncurses must be installed for this. Please do \"apt-get install libncurses5\"."
	exit
else
	echo "ncurses found!"
fi

while true; do
	read -p "Compile for Ddebug or Rrelease? (q to quit) : " dr
	case $dr in
		[Dd]* ) debug; break;;
		[Rr]* ) release; break;;
		[q]*  ) exit;;
		* ) echo "Please provide an valid answer, either d or r!";;
	esac
done
