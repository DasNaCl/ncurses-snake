/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PART_HPP
#define PART_HPP

#include <utility>

struct Part
{
public:
	Part() = default;
	virtual ~Part();

	void draw();
	virtual operator char() const = 0;

public:
	std::pair<int, int> _pos {};
};

struct Body : public Part
{
	Body() = default;
	Body(const std::pair<int, int>& pos);
	Body(const int& x, const int& y);

	operator char() const override;
};

struct Food : public Part
{
	Food() = default;
	Food(const std::pair<int, int>& pos);
	Food(const int& x, const int& y);

	operator char() const override;
};


#endif
