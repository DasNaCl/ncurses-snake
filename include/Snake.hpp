/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SNAKE_HPP
#define SNAKE_HPP

#include "Part.hpp"
#include <deque>

class Snake
{
public:
	Snake(const std::pair<int, int>& dimensions);

	void draw() const;
	bool isPointOnMe(const std::pair<int, int>& point) const;
	bool collides(const std::pair<int, int>& dimensions) const;
	bool gotFood(const Food& food);
	void moveMe(int keyval);
private:
	unsigned int _points {5U};
	char _direction {'l'};
	bool _got {false};
	std::deque<Body> _body {};
};

#endif
