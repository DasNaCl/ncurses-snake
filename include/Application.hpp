/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "Snake.hpp"
#include <string>
#include <memory>
#include <vector>

class Application
{
public:
	Application(std::size_t argc);
	~Application();

	int main(const std::vector<std::string>& arguments);

private:
	void processArguments(const std::vector<std::string>& args) const;
	void printHelp() const;
	
private:
	void drawBorder() const;
	void drawPoints() const;

	void putFood();

private:
	std::pair<int, int> _dimensions {24, 12};
	char _border {static_cast<char>(219)};
	int _delay {100000};
	std::unique_ptr<Snake> _snake {};
	Food _food {0, 0};
	int _points {0};
};

#endif
