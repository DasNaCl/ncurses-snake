/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Part.hpp"
#include <ncurses.h>

Part::~Part()
{

}

void Part::draw()
{
	move(_pos.second, _pos.first);
	addch(*this);
}

Body::Body(const std::pair<int, int>& pos) : Part()
{
	_pos = pos;
}

Body::Body(const int& x, const int& y) : Part()
{
	_pos = std::pair<int, int>(x, y);
}

Body::operator char() const
{
	return 'x';
}

Food::Food(const std::pair<int, int>& pos) : Part()
{
	_pos = pos;
}

Food::Food(const int& x, const int& y) : Part()
{
	_pos = std::pair<int, int>(x, y);
}

Food::operator char() const
{
	return '*';
}
