/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Snake.hpp"
#include <algorithm>
#include <ncurses.h>

Snake::Snake(const std::pair<int, int>& dimensions) : _points(5U), _direction('l'), _got(false), _body()
{
	for(unsigned int i = 0U; i < _points; ++i)
		_body.push_back(Body(dimensions.first/2, dimensions.second/2 + i));
}

void Snake::draw() const
{
	for(std::size_t i = 0U; i < _body.size(); ++i)
	{
		move(_body[i]._pos.second, _body[i]._pos.first);
		addch(_body[i]);
	}
}

bool Snake::isPointOnMe(const std::pair<int, int>& point) const
{
	return std::find_if(_body.cbegin(), _body.cend(),
		[&point](const Body& bodyPart)
		{return bodyPart._pos.first == point.first
		     && bodyPart._pos.second == point.second;}) != _body.cend();
}

bool Snake::collides(const std::pair<int, int>& dimensions) const
{
	return (_body.front()._pos.first <= 1 || _body.front()._pos.first >= dimensions.first-1
	     || _body.front()._pos.second <= 1 || _body.front()._pos.second >= dimensions.second-2)
	|| std::find_if(_body.cbegin()+1, _body.cend(),
	    [this](const Body& b)
	    {return b._pos.first  == _body.front()._pos.first
	         && b._pos.second == _body.front()._pos.second;}) != _body.cend();
}

bool Snake::gotFood(const Food& food)
{
	_got = _body.front()._pos.first == food._pos.first && _body.front()._pos.second == food._pos.second;
	return _got;
}

void Snake::moveMe(int keyval)
{
	switch(keyval)
	{
		case KEY_LEFT:
			if(_direction != 'r')
				_direction = 'l';
		break;
		case KEY_UP:
			if(_direction != 'd')
				_direction = 'u';
		break;
		case KEY_RIGHT:
			if(_direction != 'l')
				_direction = 'r';
		break;
		case KEY_DOWN:
			if(_direction != 'u')
				_direction = 'd';
		break;
		default: break;
	}

	if(!_got)
	{
		move(_body.back()._pos.second, _body.back()._pos.first);
		addch(' ');
		refresh();
		_body.pop_back();
	}

	if(_direction == 'l')
		_body.push_front(Body(_body.front()._pos.first-1, _body.front()._pos.second  ));
	else if(_direction == 'u')
		_body.push_front(Body(_body.front()._pos.first  , _body.front()._pos.second-1));
	else if(_direction == 'r')
		_body.push_front(Body(_body.front()._pos.first+1, _body.front()._pos.second  ));
	else if(_direction == 'd')
		_body.push_front(Body(_body.front()._pos.first  , _body.front()._pos.second+1));
}
