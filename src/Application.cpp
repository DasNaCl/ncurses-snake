/*
    Copyright (C) 2014 Matthis Kruse

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Application.hpp"
#include <ncurses.h>
#include <iostream>
#include <chrono>
#include <random>
#include <thread>

Application::Application(std::size_t argc) : _dimensions(), _border(static_cast<char>(219)), _delay(100000), _snake(), _food(), _points(0)
{
	if(argc>0)
		return;

	initscr();
	nodelay(stdscr, true);
	keypad(stdscr, true);
	noecho();
	curs_set(0);
	getmaxyx(stdscr, _dimensions.second, _dimensions.first);

	drawBorder();

	std::unique_ptr<Snake> tmp(new Snake(_dimensions));
	_snake = std::move(tmp);
	_snake->draw();

	putFood();
	drawPoints();
	refresh();
}

Application::~Application()
{
	nodelay(stdscr, false);
	mvprintw(_dimensions.second/2, _dimensions.first/2-5, "Any key...");
	refresh();
	getch();
	endwin();
}

int Application::main(const std::vector<std::string>& arguments)
{
	processArguments(arguments);
	if(!arguments.empty())
		return 0;

	auto keyPressed = getch();
	while(keyPressed != 'q')
	{
		if(_snake->collides(_dimensions))
		{
			move(_dimensions.second/2, _dimensions.first/2-4);
			printw("Game over");
			refresh();
			std::this_thread::sleep_for(std::chrono::seconds(2));
			break;
		}
		if(_snake->gotFood(_food))
		{
			putFood();
			_points += 10;
			drawPoints();
		}
		_snake->moveMe(keyPressed);
		_snake->draw();
		refresh();

		_delay = 100000 - _points * 1000;
		if(_delay < 10000)
			_delay = 10000;
		std::this_thread::sleep_for(std::chrono::microseconds(_delay));

		keyPressed = getch();
	}

	return 0;
}

void Application::processArguments(const std::vector<std::string>& args) const
{
	for(auto& a : args)
	{
		if(a == "-v"
		|| a == "--version")
		{
			std::cout << "Version 1.0, 2014 (c) Matthis Kruse" << std::endl;
		}
		else
		{
			printHelp();
		}
	}
}

void Application::printHelp() const
{
	std::cout << "snake -hv\n";
        std::cout << "Arguments:\n";
	std::cout << "\t-h --help\t\tPrint this help.\n";
	std::cout << "\t-v --version\t\tPrint version.\n";
	std::cout << std::endl;
}

void Application::drawBorder() const
{
	for(int i = 1; i < _dimensions.first-1; ++i)
	{
		move(1, i);
		addch(_border);
	}
	for(int i = 1; i < _dimensions.first-1; ++i)
	{
		move(_dimensions.second-2, i);
		addch(_border);
	}
	
	for(int i = 1; i < _dimensions.second-1; ++i)
	{
		move(i, 1);
		addch(_border);
	}
	for(int i = 1; i < _dimensions.second-1; ++i)
	{
		move(i, _dimensions.first-1);
		addch(_border);
	}
}

void Application::drawPoints() const
{
	move(_dimensions.second-1, 1);
	printw("Points: %d", _points);
}

void Application::putFood()
{
	std::mt19937 mt;
	try
	{
		std::random_device rd;
		mt = std::mt19937(rd());
	}
	catch(...)
	{
		mt = std::mt19937(std::chrono::system_clock::now().time_since_epoch().count());
	}
	std::uniform_int_distribution<int> distX(2, _dimensions.first-2);
	std::uniform_int_distribution<int> distY(2, _dimensions.second-3);

	std::pair<int, int> tmp(distX(mt), distY(mt));
	while(_snake->isPointOnMe(tmp))
	{
		tmp = std::pair<int, int>(distX(mt), distY(mt));	
	}
	_food._pos = tmp;
	_food.draw();
}
